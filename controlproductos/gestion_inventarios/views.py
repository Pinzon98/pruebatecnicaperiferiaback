import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Producto
from .utils import verificarRegistroInventario, generar_datos, verificarRegistroProducto
from .service import save, obtenerInventarios, actualizarEstadoInventario

def baseDatos(request):
    generar_datos()
    return JsonResponse({'exitoso': True})

@csrf_exempt
def registroInventario(request):
    # Se convierte el request en json
    body = json.loads(request.body)
    valido, mensaje = verificarRegistroInventario(body)
    if not valido:
        return JsonResponse({'exitoso': False, 'error': mensaje})
    save(body)
    return JsonResponse({'exitoso': True})

def obtenerTodosInventarios(request):
    inventarios = obtenerInventarios()
    if(inventarios[0]):
        return JsonResponse({'exitoso': True, 'inventarios': inventarios[1]}, safe=False)
    else:
        return JsonResponse({'exitoso': False, 'error': inventarios[1]}, status=500)

def entregaInventario(request, id):
    inventarios = actualizarEstadoInventario(id)
    if(inventarios[0]):
        return JsonResponse({'exitoso': True, 'inventarios': inventarios[1]}, safe=False)
    else:
        return JsonResponse({'exitoso': False, 'inventarios': inventarios[1], 'error': 'El inventario ya fue entregado en la fecha: ' + inventarios[2]})

def obtenerProductos(request):
    try:
        productos = Producto.objects.all()
        productos_data = [{'id': producto.id, 'nombre': producto.nombre} for producto in productos]
        return JsonResponse({'exitoso': True, 'productos': productos_data}, safe=False)
    except Exception as e:
        return JsonResponse({'exitoso': False, 'error': str(e)}, status=500)

@csrf_exempt
def registroProducto(request):
    # Se convierte el request en json
    body = json.loads(request.body)
    valido, mensaje = verificarRegistroProducto(body)
    if not valido:
        return JsonResponse({'exitoso': False, 'error': mensaje})
    if Producto.objects.filter(nombre=body['producto']).exists():
        return JsonResponse({'exitoso': False, 'error': 'El producto ya existe.'})
    nuevo_producto = Producto(nombre=body['producto'])
    nuevo_producto.save()
    return JsonResponse({'exitoso': True})
