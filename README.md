# pruebaTecnicaPeriferia

## Para empezar

- El Back esta construido en python 11
- Descargue el repositorio
- Base de Datos Relacional:
  1. Descargue Mysql
  2. Establezca una nueva conexión
  3. En nombre de la conexión ponga el nombre que guste
  4. El hostname debe ser 127.0.0.1 y el puerto 3306
  5. El usuario debe ser root y la clave 1234
  6. Ingrese a la base de datos que acaba de crear y el la parte de esquemas haga click derecho y haga click sobre crear esquema
  7. El esquema debe tener como nombre controlproductos y haga click sobre aplicar para generarlo
- Base de Datos No Relacional:
  1. Descargue MongoDb Compass
  2. Genere una nueva conexión con la url mongodb://localhost:27017
  3. Cree una nueva base de datos que tenga como nombre controlproductos
  4. Cree una nueva colección con el nombre de inventario
- Proyecto Python (Back):
  1. Ejecute el comando pip install -r requirements.txt para descargar todas las librerias que usa el proyecto
  2. Ejecute el comando python manage.py makemigrations
  3. Ejecute el comando python manage.py migrate
  4. Para ejecutar el back en la terminal debe ejecutar el comando python .\manage.py runserver